package com.jtb.wordcount;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

public class ChineseCharacterAnalysis {
		
	static HashMap<String, Integer> charfremap=new HashMap<String, Integer>();
	
	
	public void convertTextToStringArray(File file) throws IOException{
		
		BufferedReader br=new BufferedReader(new InputStreamReader(new FileInputStream(file)));
		String str=null;
		String[] rowstr=null;
		while((str=br.readLine())!=null)
		{
			rowstr=str.split(" ");
			
			
			//调用分析词频的方法将词组和出现的频率作为键值对写出map中
			/*
			 * 
			 * 一边遍历一边写hashmap 每次写的时候判断是都已经存在在hashmap中，如果有加1再写入
			 * 这个算法简单高效
			 * */
			
			for(int i=0;i<rowstr.length;i++)
			{
				int count=1;
				if(charfremap.containsKey(rowstr[i]))
				{
					count+=charfremap.get(rowstr[i]);
					charfremap.put(rowstr[i], count);
				}
				else
					{
					charfremap.put(rowstr[i], count);
					}
			}
			
			
			
		}
		System.out.println(charfremap.toString());	
	}
	
	
		//System.out.println(al);
		/*
		 * 
		 * for(int i=0;i<al.size();i++)
		{
			int frequency=1;
			for(int j=i+1;j<al.size();j++)
			{
				if((al.get(i)).equals(al.get(j)))
				{
					
					frequency++;
					al.remove(j);
					j--;//remove后指针自动指向下一个对象，在for循环后j又加1，导致指针指向了下下个对象，所以应该手动减一次
					
				}
			}
			
			if(charfremap.containsKey(strarr[i]))
			{
				frequency+=charfremap.get(strarr[i]);
				
			}
			charfremap.put(al.get(i), frequency);
			//frequency归一
			
			
		}
		
		 * 
		 * 这个算法有点逻辑问题 只能统计一行字符串的数据
		 * 多行统计有问题
		 * 
		 * */
		
			
	
	
	
}
