package com.jtb.wordcount;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Test {

    public static void main(String[] args) throws IOException {
     
    	
    	/*存储抓取的关键字数据*/
    	File f=new File("C:\\test\\textFile.txt");
		//cca.convertTextToStringArray(f);
        ChineseCharacterAnalysis cca=new ChineseCharacterAnalysis();
		cca.convertTextToStringArray(f);
		//System.out.println(cca.charfremap.size());
		
		/*下面完成排序功能*/
        Map<String, Integer> sortMap = new Test().sortMap(cca.charfremap);
        
        for(Map.Entry<String, Integer> entry : sortMap.entrySet()) {
            System.out.println(entry.getKey() + "  " + entry.getValue());
        }
    }
    
    public <K, V extends Number> Map<String, V> sortMap(Map<String, V> map) {
        class MyMap<M, N> {
            private M key;
            private N value;
            private M getKey() {
                return key;
            }
            private void setKey(M key) {
                this.key = key;
            }
            private N getValue() {
                return value;
            }
            private void setValue(N value) {
                this.value = value;
            }
        }

        List<MyMap<String, V>> list = new ArrayList<MyMap<String, V>>();
        for (Iterator<String> i = map.keySet().iterator(); i.hasNext(); ) {
            MyMap<String, V> my = new MyMap<String, V>();
            String key = i.next();
            my.setKey(key);
            my.setValue(map.get(key));
            list.add(my);
        }
        
        
        Collections.sort(list, new Comparator<MyMap<String, V>>() {
            public int compare(MyMap<String, V> o1, MyMap<String, V> o2) {
                if(o1.getValue() == o2.getValue()) {
                    return o1.getKey().compareTo(o2.getKey());
                }else{
                    return (int)(o1.getValue().doubleValue() - o2.getValue().doubleValue());
                }
            }
        });

        Map<String, V> sortMap = new LinkedHashMap<String, V>();
        for(int i = 0, k = list.size(); i < k; i++) {
            MyMap<String, V> my = list.get(i);
            sortMap.put(my.getKey(), my.getValue());
        }
        return sortMap;
    }
}