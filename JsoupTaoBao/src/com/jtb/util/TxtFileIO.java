package com.jtb.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;

public class TxtFileIO {
	
	
	
	 public static void method1(String file, String conent) {     
	        BufferedWriter out = null;     
	        try {     
	            out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, true)));     
	            out.write(conent);     
	        } catch (Exception e) {     
	            e.printStackTrace();     
	        } finally {     
	            try {     
	                if(out != null){  
	                    out.close();     
	                }  
	            } catch (IOException e) {     
	                e.printStackTrace();     
	            }     
	        }     
	    }     
	    
	    /**   
	     * 追加文件：使用FileWriter   
	     *    
	     * @param fileName   
	     * @param content   
	     */    
	    public static void method2(String fileName, String content) {   
	        FileWriter writer = null;  
	        try {     
	            // 打开一个写文件器，构造函数中的第二个参数true表示以追加形式写文件     
	            writer = new FileWriter(fileName, true);     
	            writer.write(content);       
	        } catch (IOException e) {     
	            e.printStackTrace();     
	        } finally {     
	            try {     
	                if(writer != null){  
	                    writer.close();     
	                }  
	            } catch (IOException e) {     
	                e.printStackTrace();     
	            }     
	        }   
	    }     
	    
	    /**   
	     * 追加文件：使用RandomAccessFile   
	     *    
	     * @param fileName 文件名   
	     * @param content 追加的内容   
	     */    
	    public static void method3(String fileName, String content) {   
	        RandomAccessFile randomFile = null;  
	        try {     
	            // 打开一个随机访问文件流，按读写方式     
	            randomFile = new RandomAccessFile(fileName, "rw");     
	            // 文件长度，字节数     
	            long fileLength = randomFile.length();     
	            // 将写文件指针移到文件尾。     
	            randomFile.seek(fileLength);     
	            randomFile.writeBytes(content);      
	        } catch (IOException e) {     
	            e.printStackTrace();     
	        } finally{  
	            if(randomFile != null){  
	                try {  
	                    randomFile.close();  
	                } catch (IOException e) {  
	                    e.printStackTrace();  
	                }  
	            }  
	        }  
	    }    
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/*
	 * reader,writer是对char进行操作的。
	 * inputStream,outputStream都是对于byte进行操作的。
	 * 如果是对于文件进行的操作，使用前面加上file的类.
	 * 如果数据是从外部流向内存的是inputStream,
	 * 如果数据是从内存流向外部的则使用outputStream
	 */
	/*
	 * read data from filename
	 */
	public static String readerData(String fileName) {
		String url=fileName;

		try {
			FileReader read =new FileReader(new File(url));
			StringBuffer sb=new StringBuffer();
			char ch[]= new char[1024];
			int d=read.read(ch);
			while(d!=-1){
				String str= new String(ch,0,d);
				sb.append(str);
				d =read.read(ch);
			}

			if(sb.toString().isEmpty()){
				 System.out.println(url+"is empty"); 
			}else{
				 System.out.println(sb.toString()); 
				 
			}		
			read.close();
			return sb.toString();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}
	

	/*
	 * write data into filename
	 */	
	public static void writerData(String destFileName,String contentString) {
		 File file = new File(destFileName);
	        try {
				if (file.exists()) {  
					System.out.println(destFileName+"is existed");  
					// file.delete();  
				}else{
						file.createNewFile();
				}	
				//bufferedwriter write 
				BufferedWriter bw=new BufferedWriter(new FileWriter(file));
				bw.write(contentString);//重新写文件
				bw.append(contentString);//追加文件内容
				bw.close();
				System.out.println(destFileName+"write success");  
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
	}
	
	/*
	 * 使用InPutStream  读出数据。FileInputStream是读文件的 
	 */
	public static String inputStreamData(String destFileName) {
		try {
			FileInputStream in=new FileInputStream(destFileName);
			 System.out.println("以字节为单位读取文件内容，一次读多个字节：");
			    //一次读多个字节
			 int data;
			 String str=new String();
			 while((data=in.read())!=-1){
				 System.out.print(data+"");
				 System.out.write(data);
				 str=str+data;
			 }
			 in.close();
			 System.out.println("");
			 System.out.println("data input finish");
			 System.out.print(str);	
			 
			 return str;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}
	
	/*
	 * read str into destFileName
	 */
	public static void outputStreamData(String destFileName,String str){
		try {	
			FileOutputStream output=new FileOutputStream(destFileName,true);
			byte[] b=new byte[4098];
			int len;
			//while((len =input.read(b))!=-1){}
			//output.write(intput, 0, 4098);			
			output.write(str.getBytes());
			output.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void srcTodestData(String destFileName,String srcFileName){
		try {	
			FileInputStream input=new FileInputStream(srcFileName);
			FileOutputStream output=new FileOutputStream(destFileName,true);
			byte[] b=new byte[4098];
			int len;
			while((len =input.read(b))!=-1){	
				output.write(b, 0, len);
			}			
			//output.write(str.getBytes());
			output.flush();
			output.close();
			input.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	

}
