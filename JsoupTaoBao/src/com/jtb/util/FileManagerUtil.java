package com.jtb.util;

import java.io.File;
import java.io.IOException;

public class FileManagerUtil {
	
	/*
	 * creat a file 
	 * destFileName is File's name
	 */
	public static boolean createFile(String destFileName) {		
		File file=new File(destFileName);		
		if(file.exists()){
			System.out.println("creat a file "+destFileName+"fail,this file is existed");
			return false;
		}
		if(destFileName.endsWith(File.separator)){
			System.out.println("creat a file "+destFileName+"fail,this file is not floder");
			return false;
		}
		if(!file.getParentFile().exists()){
			System.out.println("this file "+destFileName+"is not in a floder, we should creat a floder");
			if(!file.getParentFile().mkdirs()){
				System.out.println("this file "+destFileName+" in a floder fail");
				return false;
			}
		}
		//creat destFile
			try {
				if(file.createNewFile()){
					System.out.println("creat a file "+destFileName+"success");
					return true;
				}else{
					System.out.println("creat a file "+destFileName+"fail");
					return false;
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return false;
	}
	
	/*
	 * creat a dir
	 */	
	public static boolean createDir(String destDirName){
		File dir=new File(destDirName);
		if(dir.exists()){
			System.out.println("creat a file "+destDirName+"fail,this file is existed");
			return false;
		}
		if(!destDirName.endsWith(File.separator)){
			destDirName = destDirName +File.separator;
			// creat a dir
			if(dir.mkdirs()){
				System.out.println("creat a file "+destDirName+"success");
				return true;
			}else{
				System.out.println("creat a file "+destDirName+"success");
				return false;
			}
		}
		return false;		
	}
	
	/*
	 * create a file
	 */
	public static String createTempFile(String prefix,String suffix,String dirName){
		File tempFile =null;
		try{  
		    if(dirName == null) {  
		     // 在默认文件夹下创建临时文件  
		     tempFile = File.createTempFile(prefix, suffix);  
		     return tempFile.getCanonicalPath();  
		    }else{
		     File dir = new File(dirName);  
		     // 如果临时文件所在目录不存在，首先创建  
		     if(!dir.exists()) {
		      if(!createDir(dirName)){
		       System.out.println("创建临时文件失败，不能创建临时文件所在目录！");  
		       return null;
		       }  
		     }  
		     tempFile = File.createTempFile(prefix, suffix, dir);  
		     return tempFile.getCanonicalPath();
		     }  
		    } catch(IOException e) {  
		     e.printStackTrace();  
		     System.out.println("创建临时文件失败" + e.getMessage());  
		     return null;  
		    }  
	}
	/*
	 * read file or dir for list
	 */
	public static void readFileDir(String path){
		File file=new File(path);
		String str=file.getAbsolutePath();
		if(file.isFile()){
			System.out.println("当前文件名为"+str);
		}else 
			if(file.isDirectory()){
			String[] filelist=file.list();			
			if(filelist.length ==0){
				System.out.println(str+"is empty");
			}else{
				System.out.println(str+"have many childer File,list for");
				for(int i=0;i< filelist.length;i++){
					File readfile =new File(path+"\\"+filelist[i]);
					
					if(readfile.isFile()){
						 System.out.println("该文件的路径：" + readfile.getAbsolutePath());  
						 System.out.println("文件名称：" + readfile.getName());  
					}else if(readfile.isDirectory()){
						System.out.println("递归循环当前路径"+path+"\\"+filelist[i]);
						readFileDir(path+"\\"+filelist[i]);
					}				
				}
			}
		}		
	}
	
	
	
	
	
	
	/*
	 * delete all File and dir  in this path
	 */
	public static Boolean delFileORDir(String path){
		File file=new File(path);
		String str=file.getAbsolutePath();
		if(file.isFile()){
			file.delete();
		}else 
			if(file.isDirectory()){
			String[] filelist=file.list();			
			if(filelist.length ==0){
				file.delete();
			}else{
				for(int i=0;i< filelist.length; i++){
					File readfile =new File(path+"\\"+filelist[i]);					
					if(readfile.isFile()){
						 readfile.delete();  
					}else if(readfile.isDirectory()){
						delFileORDir(path+"\\"+filelist[i]);		
					}
				}
				 file.delete();
			}
			
		}
		return true;		
	}
	

}
