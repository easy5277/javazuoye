package com.jtb.util;

public class TaoShop {
	
	private String id;        //ID
	private String name;      //店名
	private String url;        //网址
	private String address;    //地址
	private String saleNum;   //售出数量
	private String onNum;     //有多少商品在售
	private String goodPing;  //好评率
	private String fuwu;      //服务态度
	private String fahuo;     //发货速度
	private String miaoshu;   //描述准确度
	private String shoucang;  //店铺收藏数
	private String startTime;  //开店时间
	
	
	public TaoShop(String id, String name, String url, String address,
			String saleNum, String onNum, String goodPing, String fuwu,
			String fahuo, String miaoshu, String shoucang, String startTime) {
		super();
		this.id = id;
		this.name = name;
		this.url = url;
		this.address = address;
		this.saleNum = saleNum;
		this.onNum = onNum;
		this.goodPing = goodPing;
		this.fuwu = fuwu;
		this.fahuo = fahuo;
		this.miaoshu = miaoshu;
		this.shoucang = shoucang;
		this.startTime = startTime;
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getUrl() {
		return url;
	}


	public void setUrl(String url) {
		this.url = url;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getSaleNum() {
		return saleNum;
	}


	public void setSaleNum(String saleNum) {
		this.saleNum = saleNum;
	}


	public String getOnNum() {
		return onNum;
	}


	public void setOnNum(String onNum) {
		this.onNum = onNum;
	}


	public String getGoodPing() {
		return goodPing;
	}


	public void setGoodPing(String goodPing) {
		this.goodPing = goodPing;
	}


	public String getFuwu() {
		return fuwu;
	}


	public void setFuwu(String fuwu) {
		this.fuwu = fuwu;
	}


	public String getFahuo() {
		return fahuo;
	}


	public void setFahuo(String fahuo) {
		this.fahuo = fahuo;
	}


	public String getMiaoshu() {
		return miaoshu;
	}


	public void setMiaoshu(String miaoshu) {
		this.miaoshu = miaoshu;
	}


	public String getShoucang() {
		return shoucang;
	}


	public void setShoucang(String shoucang) {
		this.shoucang = shoucang;
	}


	public String getStartTime() {
		return startTime;
	}


	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	
	
	
}
