package com.jtb.net;

import com.jtb.util.FileManagerUtil;
import com.jtb.util.TaoShop;
import com.jtb.util.TxtFileIO;


import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class GetList {
	
	List<TaoShop> newsItems = new ArrayList<TaoShop>();  
    private static int jishu=0;
	
	public static void main(String[] args) throws IOException, SQLException, InterruptedException
	{
		
		String dirName = "C:/test/";
		//creat dir
		FileManagerUtil.createDir(dirName);
		//creat file
		String fileName =dirName+"textFile.txt";
		FileManagerUtil.createFile(fileName);
		
		//先定义好要爬取的内容
	//	String title= null;
	//	String content= null;
	//	String website= null;
		
		//要爬虫网页的url地址
		String url="http://s.taobao.com/search?spm=a230r.7195193.0.0.b4mJfm&q=%D2%E5%CE%DA&commend=all&ssid=s5-e&sort=sale-desc&fs=1&app=shopsearch&s=";
		
		
		for(int u=0;u<=140;u++){
			
			String urlu=url+u*20;
			
			
			getData(fileName,urlu);
			
			
		}
		
		
		
	}
	
	public static void getData(String fileName,String url) throws IOException{
		
		
		       jishu++;
		       System.out.println(jishu);
		//使用jsoup工具包，要提前定义好Document类别的变量，用来存放网页html代码
				Document document = null;
				//将网页源代码装入document这个变量中
				document = Jsoup.connect(url).timeout(0).get();
			   //	Document doc = Jsoup.parse(htmlStr); 
				Element list_container=document.getElementById("list-container");
				//Elements items=document.getElementsByClass("list-item");
				Elements items=list_container.getElementsByClass("list-item");

				//Elements 此类是用来存放划分好的html内容块		
				//Elements notices = null;
				
				
				if(items!=null)
				{ 
					TaoShop taoShop=null;
					
					for (int i = 0; i < items.size(); i++) {

						Element item_el=items.get(i);
					  //  System.out.println("标题:"+i);
					    
						Element  item_el0= item_el.child(0);
						
					//	System.out.println(item_el0.toString());
						
						Elements linkStrs=item_el0.getElementsByTag("li"); 
						
						 for(int k=0;k<linkStrs.size();k++){   
							 
						      Element item_li=linkStrs.get(k);
						     	 
					            switch(k){
					            case 0:
					            	  Element  item_lia= item_li.child(0); 	
					            	  String urlNet=item_lia.getElementsByTag("a").attr("abs:href");  
					                  String title=item_lia.getElementsByTag("a").attr("title");  
					                 // System.out.println("标题:"+title+" urlNet:"+urlNet+items.size());  
					            	  
					            	break;
					            case 1:
					            	  Element  item_lih4= item_li.child(0);
					            	  
					            	  Element  item_lip= item_li.child(1);
					            	  
					            	  
					            	 //取出标签 
					            	  Element  item_lip2= item_li.child(2);
					            	  
					            	  Element item_lip22=item_lip2.child(1);
					            	  
					            	  String titleTag=item_lip22.getElementsByTag("a").text();
					            	  
					            	  System.out.println("标题TAG:"+titleTag);
					            	  
					            	  TxtFileIO.method2(fileName, titleTag);
					            	  
					            	  
					            	  //取出总的商品数，z总的销售数量
					            	  Element  item_lipsp= item_li.child(4);  
					            	  Element  item_lipsp0=item_lipsp.child(0); 
					            	  Element  item_lipsp00=item_lipsp0.child(0);
					            	  String sellSum=item_lipsp00.getElementsByTag("em").text();
					            	  System.out.println("销售数量:"+sellSum);
					            	  Element  item_lipsp1=item_lipsp.child(1); 
					            	  Element  item_lipsp10=item_lipsp1.child(0);
					            	  String sellNum=item_lipsp10.getElementsByTag("em").text();
					            	  System.out.println("在售数量:"+sellNum);
					            	  
					            	  //好评率
					            	  Element  item_lip5d= item_li.child(5); 
					            	  Element  item_lip5d0= item_lip5d.child(0);
					            	  String nicePing=item_lip5d0.getElementsByTag("div").text();  
					            	  System.out.println("好评率:"+nicePing);
					            	  //服务态度等等
					            	  Element  item_lip5d1= item_lip5d.child(1);
					            	  String niPing=item_lip5d1.getElementsByTag("div").attr("data-dsr");  
					            	  System.out.println("评率:"+niPing);
					            	  //需要使用Gson处理数据

					            	break;	

					            default:
					            	break;
					            	
					            
					            
					            
					            
					            }
					            
					            
					        }  
						
					

						 /*	  String dataUid = link.attr("data-uid");
							  String dataHref = link.attr("href");
							  String dataText = link.attr("title");
					 
						if(item_list_img.empty() != null){
							System.out.println(item_list_img.toString());
							  
							  System.out.println(item_list.toString());
						}else{
							System.out.println("null");
							  
						}
							  */ 

						
						//System.out.println(item_el.toString());
						/*
						Element total_ele0 = item_el.getElementsByClass("list-info icon-5zhe");
						String href = total_ele0.attr("href");  //网站链接
						String title_shop = total_ele0.text();
						
						System.out.println(href+"接下爱"+title_shop);
						
						*/
						
					}
					
					/*
					//抓取新闻内容 (text()方法是获取标签内的文本内容)
					notices = document.select("div[class=article-content]");
					content = notices.text();
					System.out.println("新闻内容："+content);
					
					
					//抓取新闻标题 (select()方法中可通过">"符号逐层深入标签的内部,比如：要选择标签A里面B标签的内容，写成select("A>B")即可)
					notices = document.select("ul[class=breadcrumb clearfix]>li");
					title = notices.get(4).text();
					System.out.println("新闻题目："+title);
					
					//抓取新闻url地址 (attr()方法是获取属性的内容)
					notices = document.select("head>base");
					website = notices.attr("href");
					System.out.println("新闻网址："+website);
		        	*/
				}
				
				
		
		
		
		
		
	}
	
	

}
