package javaNIO;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.CharBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;

/*
 * 创建简体中文对应的字符集对象
 * 利用字符集创建解码器
 * 利用解码器对字节码进行解码
 * 
 * 
 * */
public class FileChannelTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		File f=new File("C:\\Users\\Administrator\\Desktop\\0A008_Java编程技能训练\\java文件读写\\poets.txt");
		try(
			//创建一个文件字节输入输出流，以该文件输入流创建channel
				//获得channel的方法是通过传统节点流的getChannel（）方法
			FileChannel inChannel=new FileInputStream(f).getChannel();		
			//以文件输出流创建channel，控制文件输出
				FileChannel outChannel =new FileOutputStream("C:\\Users\\Administrator\\Desktop\\0A008_Java编程技能训练\\java文件读写\\FileChannelTest.txt").getChannel();
				)
				
				{
			//将f对应的文件利用 channel中全部数据映射成bytebuffer
			//利用CharBuffer可以直接allocate一个buffer也可以用channel映射返回一个buffer
			
			MappedByteBuffer buffer =inChannel.map(FileChannel.MapMode.READ_ONLY, 0, f.length());
			Charset charset=Charset.forName("GBK");
			//利用outchannel中write方法，将f文件中的buffer写到 poets.txt文件通过channel映射到的buffer中去
			outChannel.write(buffer);
			/*
			 * flip()方法和clear()方法都是将position置为0 但是 limit的值却不一样
			 * flip()方法令limit值先等于position，再将position=0；为输出做好准备  输出position到limit之间的值
			 * 
			 *clear()方法令limit值等于capacity 再将position-0；为写入做好准备limit设为做最大值是为了防止超出buffer大小
			 * 
			 * 
			 * 
			 * */
			
			buffer.clear();
			System.out.println(buffer);
			//创建解码器
			CharsetDecoder decoder=charset.newDecoder();
			
			CharBuffer charbuffer=decoder.decode(buffer);
			System.out.println(charbuffer);
			
	
				}
	
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}

}
