package javaNIO;

import java.nio.CharBuffer;

public class BufferTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//创建Buffer
		CharBuffer buff= CharBuffer.allocate(8);
		System.out.println("Capacity: "+buff.capacity());
		System.out.println("limit :"+buff.limit());
		System.out.println("position :"+buff.position());
		//放入元素
		buff.put('a');
		buff.put('b');
		buff.put('c');
		System.out.println("加入三个元素后，position="+buff.position());
		//调用flip()调用flip()是为取出数据做好准备      limit设为为position位置 position设置为0 使得buffer读写指针移到了开始位置
		buff.flip();
		System.out.println("执行flip()方法后limit :"+buff.limit());
		System.out.println("执行flip()方法后position :"+buff.position());
		
		//取出第一个元素
		System.out.println("第一个元素 (position =0):"+buff.get());
		System.out.println("取出第一个元素后，position="+buff.position());
		//调用clear()  clear()不是清空buffer数据 仅仅是将position置为0，将limit设置为capacity 为再次装入数据做出准备
		buff.clear();
		System.out.println("执行clear()后，limit="+buff.limit());
		System.out.println("执行clear()后，position= "+buff.position());
		System.out.println("执行clear()后，buffer中的内容并没有清楚 比如第三个元素为 ："+buff.get(2));
		System.out.println("执行绝对读取后，position= "+buff.position());
		
	}

}
