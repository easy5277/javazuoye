package javaIO;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

/*字符流读取*/

public class FileReaderTest {

	public static void main(String[] args){
		// TODO Auto-generated method stub
	//	File f=new File("C:/Users/Administrator/Desktop/云计算与应用/dataset_616605/616605/Twitter数据流一(Followered User To Following User).txt");

			try(
				FileReader fr=new FileReader("C:\\Users\\Administrator\\Desktop\\云计算与应用\\dataset_616605\\616605\\Twitter数据流一(Followered User To Following User).txt");
				//InputStreamReader isr=new InputStreamReader(fr);
				BufferedReader br=new BufferedReader(fr);)
				{
					//创建一个长度32“竹筒" 取字符
					//char[] cbr=new char[38];
					String txtread=null;
					ArrayList<String> followered=new ArrayList();
					ArrayList<String> following=new ArrayList();
					ArrayList<String> templist=new ArrayList();
					int count=2302110;
					while((txtread=br.readLine())!=null)
					{
						//System.out.println(hasread);
						String[] cbr=txtread.split(" ");
						count--;
						followered.add(cbr[0]);
						following.add(cbr[1]);
						if(count==0){
							break;
						}
						
					}
					
					String account=null;
					account=followered.get(122211);
					int firstnote,lastnote=0;
					firstnote=followered.indexOf(account);
					lastnote=followered.lastIndexOf(account);
					
					System.out.println("第一次出现的地方序号为："+(firstnote+1)+"行，最后一次出现的地方为："+(lastnote+1)+"行");
					System.out.println("followered账号："+followered.get(222334));
					System.out.println("following账号："+following.get(422223));		
					
					/*
					 * 查询一个关注他人账号的账号一共关注了多少人
					 * 也就是找关注数
					 * 
					 * */
					long time1,time2;
					time1=System.currentTimeMillis();
					account=following.get(1241);
					templist.addAll(following);
					Collections.sort(following);
					firstnote=following.indexOf(account);
					lastnote=following.lastIndexOf(account);
					System.out.println("该账号的关注数为："+(lastnote-firstnote));
					time2=System.currentTimeMillis();
					System.out.println("找关注数处理时间："+(time2-time1 ));
					
				}
		
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}

}
