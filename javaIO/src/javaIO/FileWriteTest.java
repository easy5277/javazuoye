package javaIO;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
public class FileWriteTest {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		try(
				FileReader fr=new FileReader("C:\\Users\\Administrator\\Desktop\\0A008_Java编程技能训练\\java文件读写\\极客学院账户.txt");
				FileWriter fw=new FileWriter("C:\\Users\\Administrator\\Desktop\\0A008_Java编程技能训练\\java文件读写\\newfile.txt");
				FileWriter poets=new FileWriter("C:\\Users\\Administrator\\Desktop\\0A008_Java编程技能训练\\java文件读写\\poets.txt");)
		//File newfile=new File("C:\\Users\\Administrator\\Desktop\\html5\\newfile.txt");
		{
			int hasread=0;
			char[] temp=new char[128];
			while((hasread=fr.read(temp))>0)
			{
				fw.write(temp);
			}
			
		poets.write("\n\r锦瑟-李商隐\r\n");
		poets.write("锦瑟无端五十弦，一弦一柱思华年\r\n");
		poets.write("庄生晓梦迷蝴蝶，望帝春心托杜鹃\r\n");
		poets.write("沧海月明珠有泪，蓝田玉暖玉生烟\r\n");
		poets.write("此情可待成追忆，只是当时已惘然\r\n");
		}
		
		catch(IOException e)
		{e.printStackTrace();}
	
	
	}

}
