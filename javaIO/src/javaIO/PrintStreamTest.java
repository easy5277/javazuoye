package javaIO;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

public class PrintStreamTest {

	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		try(
			FileOutputStream fos=new FileOutputStream("C:\\Users\\Administrator\\Desktop\\0A008_Java编程技能训练\\java文件读写\\poets.txt");
			PrintStream ps=new PrintStream(fos);)
			{
			//使用此处理流进行输出
			ps.println("普通字符串");
			//直接使用PrintStream输出对象
			ps.println(new PrintStreamTest());
			//将指定字符序列添加到此输出流
			/*char[] temp=new char[3];
			temp[0]='a';
			temp[1]='b';
			temp[3]='c';*/
			String temp="asdasdasdasdddddddd";
			ps.append(temp);
			//ps.close();
			
			
			
			}
			
		catch(IOException e)
		{
			e.printStackTrace();
		}
	
	
	
	}

}
