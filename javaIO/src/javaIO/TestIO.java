package javaIO;
/*字节流的读取*/
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class TestIO {

	public static void main(String[] args) {
		try (FileInputStream filein = new FileInputStream(
				"C:\\Users\\Administrator\\Desktop\\html5\\极客学院账户.txt");
				FileOutputStream fileout = new FileOutputStream(
						"C:\\Users\\Administrator\\Desktop\\html5\\copy极客学院账户.txt");) {
			byte[] buf = new byte[512];
			int hasread = 0;
			while ((hasread = filein.read(buf)) > 0) {
				fileout.write(buf, 0, hasread);

			}
		}

		catch (IOException e) {
			e.printStackTrace();

		}
	}
}
