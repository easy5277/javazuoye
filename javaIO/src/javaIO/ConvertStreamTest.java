package javaIO;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConvertStreamTest {

	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		
		/*System.in代表标准输入 即键盘输入 ，但是这个标准输入流 是InputStream 类实例 使用不方便， 
		 * 而且键盘的内容都是文本的内容，可以使用InputStreamReader转换成字符输入流 ，转换后的字符输入流是Reader 普通Reader
		 * 读取内容依然不方便，再将其打包成BufferedReader BufferedReader 中readLine()方法 可以一次读取一行内容*/
		
			try(
					//将system.in对象转换成 Reader对象
					InputStreamReader isr=new InputStreamReader(System.in);
					BufferedReader br=new BufferedReader(isr);
					)
					
					{
					String buffer=null;//这个字符串的定义还不能放在try（）里面！！！try()里面内容都是对节点流 处理流进行操作的！！
					while(true)
					{
						while((buffer=br.readLine())!=null)
					{
						//读取的字符串为exit时退出
						if(buffer.equals("exit"))
						{
							System.exit(1);
						}
						System.out.println("读取的内容为："+buffer);
					}
						}
					
				
					}
		
			catch(IOException e)
			{
				e.printStackTrace();
			}
		
	}

}
